package com.vutbr.feec.enums;

/**
 * 
 * @author Pavel Seda
 *
 */
public enum EmployeeType {
	ACTIVE, INACTIVE, DELETED;
}
