package com.vutbr.feec.model.abstractpart;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Goat extends AbstractAnimal {

	public Goat(byte age) {
		super(age);
	}

	@Override
	public void sound() {
		System.out.println("bééé");
	}

	@Override
	public String toString() {
		return "Goat [getAge()=" + getAge() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
