package com.vutbr.feec.model.abstractpart;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Pig extends AbstractAnimal {

	public Pig(byte age) {
		super(age);
	}

	@Override
	public void sound() {
		System.out.println("chro chro");
	}

	@Override
	public String toString() {
		return "Pig [getAge()=" + getAge() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
