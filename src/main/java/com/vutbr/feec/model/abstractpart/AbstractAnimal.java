package com.vutbr.feec.model.abstractpart;

/**
 * 
 * @author Pavel Seda
 *
 */
public abstract class AbstractAnimal {

	private byte age;

	public AbstractAnimal(byte age) {
		super();
		setAge(age);
	}

	public abstract void sound();

	public byte getAge() {
		return age;
	}

	public void setAge(byte age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "AbstractAnimal [age=" + age + "]";
	}

}
