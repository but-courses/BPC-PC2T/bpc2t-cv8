package com.vutbr.feec.model.abstractpart;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Cow extends AbstractAnimal {

	public Cow(byte age) {
		super(age);
	}

	@Override
	public void sound() {
		System.out.println("bůůů");
	}

	@Override
	public String toString() {
		return "Cow [getAge()=" + getAge() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
