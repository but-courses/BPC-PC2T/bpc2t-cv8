package com.vutbr.feec.model.abstractpart;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Dog extends AbstractAnimal {

	public Dog(byte age) {
		super(age);
	}

	@Override
	public void sound() {
		System.out.println("haf haf");
	}

	@Override
	public String toString() {
		return "Dog [getAge()=" + getAge() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
