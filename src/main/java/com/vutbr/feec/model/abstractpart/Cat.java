package com.vutbr.feec.model.abstractpart;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Cat extends AbstractAnimal {

	public Cat(byte age) {
		super(age);
	}

	@Override
	public void sound() {
		System.out.println("mňau");
	}

	@Override
	public String toString() {
		return "Cat [getAge()=" + getAge() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
