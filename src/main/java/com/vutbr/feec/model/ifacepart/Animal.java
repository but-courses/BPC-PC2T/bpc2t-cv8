package com.vutbr.feec.model.ifacepart;

/**
 * 
 * @author Pavel Seda
 *
 */
public interface Animal {

	void sound();
}
