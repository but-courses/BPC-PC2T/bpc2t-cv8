package com.vutbr.feec.model;

import java.util.Arrays;

import com.vutbr.feec.enums.EmployeeType;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Employee implements Comparable<Employee> {

	private String nickName;
	private String email;
	private char[] password;
	private EmployeeType employeeType;

	public Employee(String nickName, String email, char[] password, EmployeeType employeeType) {
		super();
		setNickName(nickName);
		setEmail(email);
		setPassword(password);
		setEmployeeType(employeeType);
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public EmployeeType getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}

	@Override
	public int compareTo(Employee emp) {
		return this.email.compareTo(emp.getEmail());
	}

	@Override
	public String toString() {
		return "Employee [nickName=" + nickName + ", email=" + email + ", password=" + Arrays.toString(password)
				+ ", employeeType=" + employeeType + "]";
	}

}
