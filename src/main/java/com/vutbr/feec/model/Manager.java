package com.vutbr.feec.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vutbr.feec.enums.EmployeeType;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Manager<E> extends Employee {

	private List<E> listOfRelationships = new ArrayList<>();

	public Manager(String nickName, String email, char[] password, EmployeeType employeeType) {
		super(nickName, email, password, employeeType);
	}

	public Manager(String nickName, String email, char[] password, EmployeeType employeeType,
			List<E> listOfRelationships) {
		super(nickName, email, password, employeeType);
		this.listOfRelationships = listOfRelationships;
	}

	public List<E> getListOfRelationships() {
		return listOfRelationships;
	}

	public void setListOfRelationships(List<E> listOfRelationships) {
		this.listOfRelationships = new ArrayList<>(listOfRelationships);
	}

	public void addRelationship(E e) {
		this.listOfRelationships.add(e);
	}

	@Override
	public String toString() {
		return "Manager [listOfRelationships=" + listOfRelationships + ", getNickName()=" + getNickName()
				+ ", getEmail()=" + getEmail() + ", getPassword()=" + Arrays.toString(getPassword())
				+ ", getEmployeeType()=" + getEmployeeType() + ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
