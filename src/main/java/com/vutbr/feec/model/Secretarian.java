package com.vutbr.feec.model;

import java.util.Arrays;

import com.vutbr.feec.enums.EmployeeType;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Secretarian extends Employee {

	public Secretarian(String nickName, String email, char[] password, EmployeeType employeeType) {
		super(nickName, email, password, employeeType);
	}

	@Override
	public String toString() {
		return "Secretarian [getNickName()=" + getNickName() + ", getEmail()=" + getEmail() + ", getPassword()="
				+ Arrays.toString(getPassword()) + ", getEmployeeType()=" + getEmployeeType() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
