package com.vutbr.feec.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.vutbr.feec.model.abstractpart.AbstractAnimal;

/**
 * 
 * @author Pavel Seda
 *
 */
public class WriteToFile {

	public <T extends AbstractAnimal> void writeToFile(File file, T classExtendingAbstractAnimal) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(classExtendingAbstractAnimal.toString());
		} catch (IOException io) {
			// handle exception (log or re-throw)
			io.printStackTrace();
		}
	}
}
