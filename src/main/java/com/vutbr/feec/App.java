package com.vutbr.feec;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.vutbr.feec.enums.EmployeeType;
import com.vutbr.feec.io.WriteToFile;
import com.vutbr.feec.model.Employee;
import com.vutbr.feec.model.Manager;
import com.vutbr.feec.model.Secretarian;
import com.vutbr.feec.model.abstractpart.AbstractAnimal;
import com.vutbr.feec.model.abstractpart.Cat;
import com.vutbr.feec.model.abstractpart.Cow;
import com.vutbr.feec.model.abstractpart.Dog;
import com.vutbr.feec.model.abstractpart.Goat;
import com.vutbr.feec.model.abstractpart.Pig;
import com.vutbr.feec.model.ifacepart.Animal;
import com.vutbr.feec.model.ifacepart.CatImpl;
import com.vutbr.feec.model.ifacepart.CowImpl;
import com.vutbr.feec.model.ifacepart.DogImpl;
import com.vutbr.feec.model.ifacepart.GoatImpl;
import com.vutbr.feec.model.ifacepart.PigImpl;

/**
 * 
 * @author Pavel Seda
 *
 */
public class App {

	private static final String MAVEN_RESOURCES_PREFIX = "./src/main/resources/";

	public static void main(String[] args) {

		employeesPart();
		abstractAnimalPart();

		System.out.println("");

		interfacePart();
		writeToFile();
	}

	public static void employeesPart() {
		Employee emp1 = new Employee("SedaQ", "pavelseda@email.cz", "test".toCharArray(), EmployeeType.ACTIVE);
		Employee emp2 = new Employee("SedaQ2", "pavelseda2@email.cz", "test".toCharArray(), EmployeeType.ACTIVE);
		Employee emp3 = new Employee("SedaQ3", "pavelseda3@email.cz", "test".toCharArray(), EmployeeType.ACTIVE);
		Employee emp4 = new Employee("SedaQ4", "pavelseda4@email.cz", "test".toCharArray(), EmployeeType.ACTIVE);
		Employee emp5 = new Employee("SedaQ5", "pavelseda5@email.cz", "test".toCharArray(), EmployeeType.ACTIVE);

		Employee secretarian1 = new Secretarian("Sec1", "pavelseda5@email.cz", "test".toCharArray(),
				EmployeeType.ACTIVE);
		Employee secretarian2 = new Secretarian("Sec2", "pavelseda5@email.cz", "test".toCharArray(),
				EmployeeType.ACTIVE);

		Manager<Employee> manager = new Manager<Employee>("EpicManager", "pavelseda5@email.cz", "test".toCharArray(),
				EmployeeType.ACTIVE);
		manager.setListOfRelationships(Arrays.asList(emp1, emp2, emp3, emp4, emp5, secretarian1, secretarian2));

		List<Employee> managerEmployees = manager.getListOfRelationships();
		for (Employee employee : managerEmployees) {
			if (employee instanceof Secretarian) {
				System.out.println(employee);
			}
		}
	}

	public static void abstractAnimalPart() {
		AbstractAnimal cat = new Cat((byte) 15);
		AbstractAnimal cow = new Cow((byte) 17);
		AbstractAnimal dog = new Dog((byte) 17);
		AbstractAnimal pig = new Pig((byte) 15);
		AbstractAnimal goat = new Goat((byte) 65);

		cat.sound();
		cow.sound();
		dog.sound();
		pig.sound();
		goat.sound();
	}

	public static void interfacePart() {
		Animal cat = new CatImpl();
		Animal cow = new CowImpl();
		Animal dog = new DogImpl();
		Animal pig = new PigImpl();
		Animal goat = new GoatImpl();

		cat.sound();
		cow.sound();

		dog.sound();
		pig.sound();
		goat.sound();
	}

	private static void writeToFile() {
		File file = new File(MAVEN_RESOURCES_PREFIX + "animals.txt");
		WriteToFile writeToFile = new WriteToFile();
		writeToFile.writeToFile(file, new Cat((byte) 17));
	}

}
