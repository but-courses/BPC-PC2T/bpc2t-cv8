package com.vutbr.feec.repository;

import com.vutbr.feec.model.abstractpart.AbstractAnimal;

/**
 * 
 * V této třídě je ukázka generické metody.
 * 
 * @author Pavel Seda
 *
 */
public class AuditService {

	public <T extends AbstractAnimal> void save(T someAnimal) {
		System.out.println(
				"Vstupní generický parametr T může být pouze typu třídy, která dědí ze třídy AbstractAnimal, pro demonstraci ukazuji, že lze zavolat metoda getAge()."
						+ someAnimal.getAge());
	}

}
