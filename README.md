# Course: BPC2T - seminar 8 
This project provides a solution for seminar 8. Includes topics like:
1. Class creation 
2. Enumerations
3. Interface and Abstract classes 
4. Java docs (@author tag, @param etc.)
5. Implementing toString method
6. Loop constructors
7. Generic classes and methods

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```